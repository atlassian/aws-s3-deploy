# Bitbucket Pipelines Pipe: AWS S3 deploy

Pipe to deploy to [Amazon S3][Amazon S3].
Syncs directories and S3 prefixes. Recursively copies new and updated files from the source local directory to the destination. Only creates folders in the destination if they contain one or more files.

This pipe can be used together with the [aws-cloudfront-invalidate][aws-cloudfront-invalidate] when it is required to refresh the
CDN caches after new files are uploaded to S3.


Now pipe uses [AWS CLI version 2][AWS CLI version 2]. AWS CLI version 2 more strict for filenames/[key-names][Creating object key names]: only utf-8 supported.
Other breaking changes could be found on the [AWS CLI version 2 migration][AWS CLI version 2 migration] page.


**Note**: Breaking changes were introduced in version 2.0.0. Please review the current version of the Pipe and update your configuration accordingly before upgrading.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:
    
```yaml
- pipe: atlassian/aws-s3-deploy:2.0.1
  variables:
    AWS_ACCESS_KEY_ID: '<string>' # Optional. Required for default authentication.
    AWS_SECRET_ACCESS_KEY: '<string>' # Optional. Required for default authentication.
    AWS_DEFAULT_REGION: '<string>' # Optional if already defined in the context.
    AWS_OIDC_ROLE_ARN: '<string>' # Optional. Required for OpenID Connect (OIDC) authentication.
    AWS_ROLE_ARN: '<string>' # Optional. Required for STS authentication.
    AWS_ROLE_SESSION_NAME: '<string>' # Optional. Required for STS authentication.
    S3_BUCKET: '<string>'
    LOCAL_PATH: '<string>'
    # CONTENT_ENCODING: '<string>' # Optional.
    # ACL: '<string>' # Optional.
    # STORAGE_CLASS: '<string>' # Optional.
    # CACHE_CONTROL: '<string>' # Optional.
    # EXPIRES: '<timestamp>' # Optional.
    # DELETE_FLAG: '<boolean>' # Optional.
    # EXTRA_ARGS: '<string>' # Optional.
    # PRE_EXECUTION_SCRIPT: '<string>' # Optional.
    # DEBUG: '<boolean>' # Optional.
```

## Variables

| Variable                  | Usage                                                                                                                                                                                                                                                                         |
|---------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| AWS_ACCESS_KEY_ID (1)     | AWS access key.                                                                                                                                                                                                                                                               |
| AWS_SECRET_ACCESS_KEY (1) | AWS secret key.                                                                                                                                                                                                                                                               |
| AWS_DEFAULT_REGION (**)   | The AWS region code (us-east-1, us-west-2, etc.) of the region containing the AWS resource(s). For more information, see [Regions and Endpoints][Regions and Endpoints] in the _Amazon Web Services General Reference_.                                                       |
| AWS_OIDC_ROLE_ARN (1)     | The ARN of the role used for web identity federation or OIDC. See **Authentication**.                                                                                                                                                                                         |
| AWS_ROLE_ARN (1)          | Specifies the Amazon Resource Name (ARN) of an IAM role with a web identity provider that you want to use to run the AWS commands. See **Authentication**.                                                                                                                    |
| AWS_ROLE_SESSION_NAME (1) | Specifies the name to attach to the role session. This value is provided to the RoleSessionName parameter when the AWS CLI calls the AssumeRole operation, and becomes part of the assumed role user ARN: arn:aws:sts::123456789012:assumed-role/role_name/role_session_name. |  
| S3_BUCKET (*)             | S3 bucket name or path-like directory structure.                                                                                                                                                                                                                              |
| LOCAL_PATH (*)            | Local path to folder to be deployed.                                                                                                                                                                                                                                          |
| CONTENT_ENCODING          | Content encodings that have been applied to the object.                                                                                                                                                                                                                       |
| ACL                       | ACL for the object when the command is performed. Valid values are: `private`, `public-read`, `public-read-write`, `authenticated-read`, `bucket-owner-read`, `bucket-owner-full-control`. Default: `private`.                                                                |
| STORAGE_CLASS             | Type of storage to use for the object. Valid options are `STANDARD`, `REDUCED_REDUNDANCY`, `STANDARD_IA`, `ONEZONE_IA`. Default: `STANDARD`.                                                                                                                                  |
| CACHE_CONTROL             | Caching behavior along the request/reply chain. Valid options are `no-cache`, `no-store`, `max-age=<seconds>`, `s-maxage=<seconds> no-transform`, `public`, `private`. Defaults to unset.                                                                                     |
| EXPIRES                   | Date and time at which the object is no longer cacheable. ISO 8601 format: `YYYY-MM-DDThh:mm:ssTZD`. Defaults to unset.                                                                                                                                                       |
| DELETE_FLAG               | Destination path is cleaned up before the upload. Default: `false`.                                                                                                                                                                                                           |
| EXTRA_ARGS                | Extra arguments to be passed to the CLI (see AWS docs for more details). Defaults to unset.                                                                                                                                                                                   |
| PRE_EXECUTION_SCRIPT      | Path to pre-execution script to execute additional specific actions needed.                                                                                                                                                                                                   |
| DEBUG                     | Turn on extra debug information. Default: `false`.                                                                                                                                                                                                                            |
_(*) = required variable. This variable needs to be specified always when using the pipe._
_(**) = required variable. If this variable is configured as a repository, account or environment variable, it doesn’t need to be declared in the pipe as it will be taken from the context. It can still be overridden when using the pipe._
_(1) = Choose one of the options: (`AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`) or `AWS_OIDC_ROLE_ARN` or (`AWS_ROLE_ARN` and `AWS_ROLE_SESSION_NAME`)._

More info about parameters and values can be found in the AWS official documentation: https://docs.aws.amazon.com/cli/latest/reference/s3/sync.html


## Authentication


Supported options:

1. Environment variables: AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY. Default option.
   
2. Assume role provider with OpenID Connect (OIDC). More details in the Bitbucket Pipelines Using OpenID Connect guide [Integrating aws bitbucket pipeline with oidc][aws-oidc]. Make sure that you setup OIDC before:
    * configure Bitbucket Pipelines as a Web Identity Provider in AWS
    * attach to provider your AWS role with required policies in AWS
    * setup a build step with `oidc: true` in your Bitbucket Pipelines
    * pass AWS_OIDC_ROLE_ARN (*) variable that represents role having appropriate permissions to execute actions on AWS S3

3. Request temporary credentials from AWS STS by assuming an IAM role with a web identity provider that you want to use to run the AWS commands.
    * set the ARN of the role to assume through `AWS_ROLE_ARN`
    * set session name (associated with a temporary credentials from AWS STS) through `AWS_ROLE_SESSION_NAME`


## Examples

### Basic example:
Deploy files to S3.
```yaml
script:
  - pipe: atlassian/aws-s3-deploy:2.0.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      S3_BUCKET: 'my-bucket-name'
      LOCAL_PATH: 'build'
```

Deploy files to a directory named `logs` inside an S3 bucket my-bucket-name.
```yaml
script:
  - pipe: atlassian/aws-s3-deploy:2.0.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      S3_BUCKET: 'my-bucket-name/logs'
      LOCAL_PATH: '$(pwd)'
```

Deploy files to S3. `AWS_DEFAULT_REGION` is configured as repository variable, so there is no need to declare it in the pipe.
```yaml
script:
  - pipe: atlassian/aws-s3-deploy:2.0.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      S3_BUCKET: 'my-bucket-name'
      LOCAL_PATH: 'build'
```

### Advanced example: 

Deploy files to S3 with OpenID Connect (OIDC) alternative authentication without required `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`.
Parameter `oidc: true` in the step configuration and variable `AWS_OIDC_ROLE_ARN` are required:

```yaml
- step:
    oidc: true
    script:
      - pipe: atlassian/aws-s3-deploy:2.0.1
        variables:
          AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
          AWS_OIDC_ROLE_ARN: 'arn:aws:iam::123456789012:role/role_name'
          S3_BUCKET: 'my-bucket-name'
          LOCAL_PATH: 'build'
```

Deploy files to S3 with AWS STS alternative authentication. You still have to provide `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` to acquire temporary credentials.

```yaml
script:
  - pipe: atlassian/aws-s3-deploy:2.0.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      AWS_ROLE_ARN: "arn:aws:iam::012349999999:role/my-assumed-role"
      AWS_ROLE_SESSION_NAME: "my-assumed-role"
      S3_BUCKET: 'my-bucket-name'
      LOCAL_PATH: 'build'
```

Deploy files to S3 with preexecution hook sh script. NOTE: be aware that script to be executed should have permissions at least read and execute permissions.
```yaml
script:
  - echo 'aws configure max_queue_size 500' > .my-script.sh
  - chmod 005 .my-script.sh
  - pipe: atlassian/aws-s3-deploy:2.0.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      S3_BUCKET: 'my-bucket-name/logs'
      LOCAL_PATH: '$(pwd)'
      PRE_EXECUTION_SCRIPT: '.my-script.sh'
```

Deploy files to S3 with extra fields:

```yaml
script:
  - pipe: atlassian/aws-s3-deploy:2.0.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      S3_BUCKET: 'my-bucket-name'
      LOCAL_PATH: 'build'
      ACL: 'public-read'
      CACHE_CONTROL: 'max-age=3600'
      EXPIRES: '2018-10-01T00:00:00+00:00'
      DELETE_FLAG: 'true'
      EXTRA_ARGS: '--follow-symlinks --quiet'
```

Example with an EXTRA_ARGS to include only files with '.txt' extension for deploy.
```yaml
script:
  - pipe: atlassian/aws-s3-deploy:2.0.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      S3_BUCKET: 'my-bucket-name'
      LOCAL_PATH: 'build'
      EXTRA_ARGS: "--exclude=* --include=*.txt"
```

Example with an EXTRA_ARGS to exclude all files with '.txt' extension from deploy.
```yaml
script:
  - pipe: atlassian/aws-s3-deploy:2.0.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      S3_BUCKET: 'my-bucket-name'
      LOCAL_PATH: 'build'
      EXTRA_ARGS: "--exclude=*.txt"
```

Example using an `aws-s3-deploy` pipe to sync your files to S3 and triggering a distribution invalidation to refresh the CDN caches with `aws-cloudfront-invalidate` pipe:
```yaml
script:
  - pipe: atlassian/aws-s3-deploy:2.0.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      S3_BUCKET: 'my-bucket-name'
      LOCAL_PATH: 'build'

  - pipe: atlassian/aws-cloudfront-invalidate:0.1.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      DISTRIBUTION_ID: '123xyz'
```


## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce


## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-questions?add-tags=bitbucket-pipelines,pipes,aws,s3
[aws-oidc]: https://support.atlassian.com/bitbucket-cloud/docs/deploy-on-aws-using-bitbucket-pipelines-openid-connect
[AWS CLI version 2]: https://docs.aws.amazon.com/cli/latest/userguide/welcome-versions.html
[Creating object key names]: https://docs.aws.amazon.com/AmazonS3/latest/userguide/object-keys.html
[AWS CLI version 2 migration]: https://docs.aws.amazon.com/cli/latest/userguide/cliv2-migration.html
[Amazon S3]: https://docs.aws.amazon.com/cli/latest/reference/s3/sync.html
[aws-cloudfront-invalidate]: https://bitbucket.org/atlassian/aws-cloudfront-invalidate
[Regions and Endpoints]: https://docs.aws.amazon.com/general/latest/gr/rande.html
