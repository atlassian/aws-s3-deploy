FROM amazon/aws-cli:2.24.17

# install jq utility to parse the json when 'sts' authentication is used
RUN yum -y update \
    && yum -y install jq-1.5 \
    && yum clean all \
    && rm -rf /var/cache/yum

# install requirements
RUN curl -fsSL -o /common.sh https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.6.0/common.sh

# copy the pipe source code
COPY LICENSE.txt README.md pipe.yml /
COPY pipe /

ENTRYPOINT ["/pipe.sh"]
