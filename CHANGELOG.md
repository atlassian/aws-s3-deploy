# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 2.0.1

- patch: Internal maintenance: Bump base docker image to 2.24.17.
- patch: Internal maintenance: Bump pipes versions in pipelines configuration file.

## 2.0.0

- major: Update pipe logic for bash strict mode usage. Remove AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY from default repository variables. Breaking change! You have to update your pipe variables. Take a look at the README for an additional information.

## 1.6.2

- patch: Internal maintenance: Bump pipes versions in pipelines configuration file. 
- patch: Internal maintenance: Update docker image to version amazon/aws-cli:2.22.22.

## 1.6.1

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 1.6.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 1.5.0

- minor: Bump docker image amazon/aws-cli version to 2.13.37
- minor: Implemented support for AWS STS (using assume-role functionality).

## 1.4.1

- patch: Update documentation: clarifying the default value to unset for CACHE_CONTROL variable.

## 1.4.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 1.3.0

- minor: Internal maintenance: Update bitbucket-pipes-toolkit.

## 1.2.0

- minor: Internal maintenance: Bump version of the base Docker image to amazon/aws-cli:2.11.11.
- patch: Internal maintenance: Bump version of boto3 and other dependencies.
- patch: Internal maintenance: Bump version of the bitbucket-pipe-release.
- patch: Internal maintenance: Update the community link.

## 1.1.0

- minor: Support pre-execution script before executing pipe.

## 1.0.1

- patch: Add details about breaking changes during migration aws-cli from v1 to v2. Aws-cli v2 allow only utf-8 encoded filenames/key-names.

## 1.0.0

- major: Migrate aws-cli to v2. Contains breaking changes https://docs.aws.amazon.com/cli/latest/userguide/cliv2-migration.html. Pipe based on docker image amazon/aws-cli:2.2.13.

## 0.5.0

- minor: Bump aws-cli -> 1.18.188.
- minor: Support AWS OIDC authentication. Environment variables AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY are not required anymore.

## 0.4.5

- patch: Internal maintenance: bump bitbucket-pipe-release.

## 0.4.4

- patch: Internal maintenance: change pipe metadata according to new structure

## 0.4.3

- patch: Update the Readme with an example path-like directory structure.

## 0.4.2

- patch: Internal maintenance: Add gitignore secrets.

## 0.4.1

- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.
- patch: Update the Readme with details about default variables.

## 0.4.0

- minor: Add default values for AWS variables.

## 0.3.8

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 0.3.7

- patch: Update Readme with examples exclude-include files.

## 0.3.6

- patch: Internal maintenance: Add auto infrastructure for tests.

## 0.3.5

- patch: Updated the docs to mention the aws-cloudfront-invalidate pipe

## 0.3.4

- patch: Internal release to rewrite tests in python

## 0.3.3

- patch: Internal release

## 0.3.2

- patch: Refactor pipe code to use pipes bash toolkit.
- patch: Update aws-cli base docker image version.

## 0.3.1

- patch: Fix validation message if directory does'nt exist

## 0.3.0

- minor: Change default environment variable BITBUCKET_REPO_OWNER to BITBUCKET_WORKSPACE due to deprecation in Bitbucket API.

## 0.2.4

- patch: Added contribution guidelines
- patch: Updated contributing guidelines

## 0.2.3

- patch: FIX issue with large writes to stdout failing with 'Resource temporarily unavailable'

## 0.2.2

- patch: Standardising README and pipes.yml.

## 0.2.1

- patch: Fix README.md typo.

## 0.2.0

- minor: Allow debugging of the aws s3 command via the DEBUG variable.
- minor: Convert from tasks to pipes.

## 0.1.3

- patch: Use quotes for all pipes examples in README.md.

## 0.1.2

- patch: Remove details

## 0.1.1

- patch: Restructure README.md to match user flow.

## 0.1.0

- minor: Initial release of Bitbucket Pipelines S3 deploy pipe.
